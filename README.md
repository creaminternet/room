# Meeting Room application for PHP #

This application connects to Google Calendar with an API and display the meeting room status. It can be used on a tablet, smartphone or any other screen to display the status of your meeting room.

# Installation #

## Manually ##

To install this language package manually you need access to your server file system and you need access to the command line of your server. And take the following steps:

* [Download the zip file from the Bitbucket repository](https://bitbucket.org/creamlabs/room/downloads) and unpack it. 
* Create a Google API user and fill in the config.php variables.

# Release Notes #

## Release 2.0.0

- Improvement: Removed old Google API library and updated to latest release with Composer.
- Improvement: Updated script to be compatible with latest Google API library.

# Roadmap #

Things still on the todo list:

* Create an index.php to list all the rooms.
* Create a book.php which can be pushed to book the room for half an hour.
* Use classes instead of procedural coding.
* Use templates instead of mixing business logic with HTML. 
* Display gravatars of the users involved in the meeting.

# Contribute #

To help push the 'Room' forward please fork the [Bitbucket repository](https://bitbucket.org/creamlabs/room/overview) and submit a pull request with your changes. 

# License #

This module is distributed under the following licenses:

* Academic Free License ("AFL") v. 3.0
* Open Software License v. 3.0 (OSL-3.0)

# Authors #

* [Cream ](https://www.cream.nl/) created and released this module for PHP. It is based on MEAT the [meeting room application from Atlassian](http://blogs.atlassian.com/2012/12/meat-one-tribes-quest-for-meeting-room-transparency/).