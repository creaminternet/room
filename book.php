<?php

// Set error reporting, surpress warnings thrown by Google API.
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);

// require_once 'src/Google/autoload.php';
require_once './vendor/autoload.php';
require_once './config.php';

date_default_timezone_set('Europe/Amsterdam');

$client = new Google\Client();
$client->setApplicationName("Room");
$client->setScopes([Google\Service\Calendar::CALENDAR]);
$client->setAuthConfig('./tmp/calendar-8a9370bb6ee4.json');
$client->fetchAccessTokenWithAssertion();

$start = new Google\Service\Calendar\EventDateTime();
$start->setDateTime(date("c", time()));

$end = new Google\Service\Calendar\EventDateTime();
$end->setDateTime(date("c", time()+1800));

$event = new Google\Service\Calendar\Event();
$event->summary = 'IN: Meeting';
$event->setEnd($end);
$event->setStart($start);

$service = new Google\Service\Calendar($client);
$service->events->insert("creaminternetbureau@gmail.com", $event);

header("Location: /room.php");
