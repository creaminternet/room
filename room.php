<?php

// Set error reporting, surpress warnings thrown by Google API.
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);

// require_once 'src/Google/autoload.php';
require_once './vendor/autoload.php';
require_once './config.php';

date_default_timezone_set('Europe/Amsterdam');

$client = new Google\Client();
$client->setApplicationName("Room");
$client->setScopes([Google\Service\Calendar::CALENDAR_READONLY]);
$client->setAuthConfig('./tmp/calendar-8a9370bb6ee4.json');
$client->fetchAccessTokenWithAssertion();

$service = new Google\Service\Calendar($client);

$calendarList = $service->calendarList->listCalendarList();
$displayEvents = array();
$persons = array();

while(true) {
    foreach ($calendarList->getItems() as $calendarListEntry) {
        $start = date('Y-m-d')."T00:00:00-00:00";
        $end = date('Y-m-d')."T23:59:59-00:00";

        // get events
        try {
            $events = $service->events->listEvents($calendarListEntry->id, array('timeMin' => $start, 'timeMax' => $end, 'showDeleted' => 'false','orderby','q' => 'IN:'));
        } catch (Exception $e) {
            break;
        }

        foreach ($events->getItems() as $event) {
            if($event->getStatus() != 'cancelled') {
                $displayEvents[strtotime($event->getStart()->dateTime)] = $event;
                $persons[strtotime($event->getStart()->dateTime)][] = $calendarListEntry->getSummary();
            }
        }
    }
    $pageToken = $calendarList->getNextPageToken();
    if ($pageToken) {
        $optParams = array('pageToken' => $pageToken);
        $calendarList = $service->calendarList->listCalendarList($optParams);
    } else {
        break;
    }
}

ksort($displayEvents);

$now = time();
$color = "green";
$appointments = "";
$appointmentNow = "";
foreach ($displayEvents as $key => $event) {
    $end = strtotime($event->getEnd()->dateTime);
    $display = true;

    // Display not available 5 minutes before actual start of meeting
    if($key-($now+(5*60)) < 0) {
        // Start afsrpaak is voorbij
        if($end-$now > 0) {
            // Afspraak is bezig
            $color = "red";
            $display = false;

            $appointmentNow .= '<h1 class="text-center">'.str_replace('IN: ', "", $event->getSummary()) . "</h1>";
            $appointmentNow .= '<div class="text-center hero__navigation appointment-info">'.date('H:i', $key) . " - " . date('H:i', $end) . ", " . implode(' & ', $persons[$key]) . "</div>";
        } else {
            // Afspraak is voorbij
            $display = false;
        }
    } elseif($key-$now < 1800 && $color != 'red') {
        // Afspraak is binnen een half uur
        $color = 'orange';
        $display = false;
        $meetingInMinutes = round((($key - $now) / 60));

        $appointmentNow .= '<div class="text-center hero__title appointmentnow-minutes">in '.$meetingInMinutes .' min.</div>';
        $appointmentNow .= '<h1 class="text-center">'.str_replace('IN: ', "", $event->getSummary()) . "</h1>";
        $appointmentNow .= '<div class="text-center hero__navigation appointment-info">'.date('H:i', $key) . " - " . date('H:i', $end) . ", " . implode(' & ', $persons[$key]) . "</div>";
    }

    if($display) {

        $appointments .= '<div class="hero__meta upcomming">' . '<span class="hero__meta-item hero__date">'.date('H:i', $key) . " - " . date('H:i', $end) . '</span>' . '<span class="hero__meta-item hero__tags">' . str_replace('IN: ', "", $event->getSummary()) . '</span>' . '<span class="hero__meta-item hero__author">' . implode(' & ', $persons[$key]) . '</span>' . '</div>';
    }
}

if ($color == "green") {
    $bgcolor = "rgba(0, 128, 0, 0.75)";
}

if ($color == "orange") {
    $bgcolor = "rgba(255, 165, 0, 0.75)";
}

if ($color == "red") {
    $bgcolor = "rgba(230, 0, 126, 0.75)";
}

?>

<html class="pl">
<head>
    <title>Room</title>

    <meta http-equiv="refresh" content="30" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body class="body">
<div class="hero hero--home" style="background-image: url('https://www.cream.nl/wp-content/uploads/2017/01/AdobeStock_104374161_blackwhite-1024x769.jpg');">
    <div class="hero__content" style="background: <?php echo $bgcolor; ?>;">
        <h1 class="hero__title">
            <?php if ($appointmentNow){
                print $appointmentNow;
            } else {
                if ($color == 'orange') { ?>
                    <?php print $meetingInMinutes; ?>
                    <?php
                } else {
                    ?>
                    Beschikbaar
                <?php }
            } ?>
        </h1>
        <?php if ($appointments != "") { ?>
            <div class="upcomming-events">
                <div class="hero__paragraph upcomming">
                    Upcoming events
                </div>
                <?php print $appointments ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="footer-info">
    <div>
        <svg class="logo--flat" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 628.8 167.48" width="200" height="100">
            <path d="M257,58c2.08-5.56-.52-9.55-4.51-12-6.77-3.82-15.62-5-22.74-5-24.13,0-36.46,18.4-36.46,40.79V93.93c0,22.74,10.59,39.41,36.28,39.41,7.29,0,15.28-1,22.57-4.86,4-2.25,6.59-6.25,4.69-12a8.47,8.47,0,0,0-11.63-5.21,33,33,0,0,1-14.06,3c-9.55,0-14.41-7.29-14.41-19.1v-16c0-12,5.73-19.1,15.27-19.1a29.08,29.08,0,0,1,13.19,3A8.67,8.67,0,0,0,257,58Zm76-1.74c1.39-4.68.18-9.72-4.17-12.5a20.47,20.47,0,0,0-10.76-2.6c-5.55,0-16.15,1.21-24.13,10.94-2.26-4.34-6.25-8-12.67-9.55a13,13,0,0,0-3.64-.35C270.54,42.2,266,45,266,51.75a8.42,8.42,0,0,0,3.13,6.77c2.78,2.43,5.21,4,5.21,9.9v53.29a11.72,11.72,0,0,0,23.43,0V73.45c2.08-7.29,8-10.07,14.93-10.07a28.87,28.87,0,0,1,7.46.69C327,65.46,331.48,61.47,333,56.26Zm77,29.86V78.65c0-20.14-9.89-37.67-36.28-37.67-25.34,0-36.63,17.88-36.63,38.19V95.32c0,20.31,9.55,38,36.46,38a64.86,64.86,0,0,0,27.25-5.55c4.34-1.91,6.42-6.25,4.86-11.46a8.48,8.48,0,0,0-11.29-5.91A45.59,45.59,0,0,1,375,114.24c-9.72,0-14.41-6.08-14.41-16.49v-3.3h41.49A8.15,8.15,0,0,0,410.07,86.12ZM387.5,74.66v2.26H360.6V74.66c0-9.89,4.51-15.45,13.71-15.45S387.5,64.77,387.5,74.66Zm108.41,49.13a8.9,8.9,0,0,0-3.3-6.94c-2.6-2.26-4.51-3.82-4.51-9.72v-29c0-26-9.55-37.15-34.72-37.15-9.2,0-20.14,1.56-27.6,6.42-10.59,6.77-1.39,21.87,10.07,16a30.28,30.28,0,0,1,15.62-3.82c10.59,0,13.19,5.73,13.19,15.8v3.3H449.21c-22.22,0-34.37,10.07-34.37,26.39v1.22c0,16.32,10.42,27.08,31.25,27.08,7.64,0,17.19-1.56,23.78-7.29a17.29,17.29,0,0,0,3.47,3.64,18,18,0,0,0,10.77,3.65C491.23,133.34,495.91,130.56,495.91,123.79Zm-31.25-20.66c0,9.2-5.56,13.54-13.54,13.54-9,0-13-3.65-13-10.07v-1.22c0-5.73,3.64-9.89,13.88-9.89h12.67ZM628.8,121.71V73.62c0-19.62-7.29-32.64-29-32.64-11.63,0-20.31,3.64-26.39,9.89C568.9,44.63,561.61,41,550.16,41c-11.11,0-19.61,3.47-25.69,9a18.12,18.12,0,0,0-4-4.17,18.79,18.79,0,0,0-10.94-3.64c-6.77,0-11.63,2.78-11.63,9.55A8.42,8.42,0,0,0,501,58.52c2.78,2.43,5.21,4,5.21,9.9v53.29a11.72,11.72,0,0,0,23.43,0V69.8A17.06,17.06,0,0,1,543.73,63c10.07,0,12.15,6.42,12.15,13.54v45.13a11.63,11.63,0,0,0,11.63,11.63,11.78,11.78,0,0,0,11.81-11.63V73.62c0-1.22-.17-2.43-.17-3.47A16.94,16.94,0,0,1,593.38,63c10.07,0,12,6.42,12,13.54v45.13a11.72,11.72,0,0,0,23.44,0Z" fill-rule="evenodd"></path>
            <path d="M83.74,0a83.74,83.74,0,1,0,83.74,83.74A83.74,83.74,0,0,0,83.74,0Zm71.59,86.38A20.66,20.66,0,0,0,155,83.6a118.9,118.9,0,0,0-4.56-15.79c-8.64-25-30.58-45.13-57.22-49C63.11,14.56,32.61,33.09,25.28,63.59,19.48,87.14,30.68,113,53.07,122.27c15.44,6.56,34.75,3.86,46.33-9.27a30.18,30.18,0,0,0,7.72-21.62c-.26-7.78-3.09-15.06-9.26-18.14,2.7,10.81,3.09,23.16-5.79,31.65-9.65,9.27-23.93,10.42-35.52,3.48-15.06-8.88-20.46-27.8-15.06-43.62A42.13,42.13,0,0,1,57.32,43.9C75.46,30.77,99,33.48,115.61,47c23.93,19.3,26.63,54,8.11,78.36-15.44,20.08-39.38,29.34-64.47,24.71-19.69-3.47-36.29-15.83-47.1-32,18.53,26.63,54,39,84.93,25.86,32-13.51,47.48-55.59,25.09-84.93A48.62,48.62,0,0,0,87.43,40C67,38.5,45.35,54.71,45.74,76.33c.38,10.42,5.79,21.23,15.83,25.1,4.63,1.54,10.81,3.08,14.28-.78-7.68-3.28-13.9-7.72-16.6-16.21a20.33,20.33,0,0,1,4.25-19.69C72.37,54.32,87.43,53.17,99,60.11c13.12,8.11,18.53,23.55,15.06,38.6-4.25,17.76-19.69,29.72-37.06,31.65a49.38,49.38,0,0,1-45.94-20.46C21.42,96.4,18.33,81,21.8,64.75,26,45.06,39.56,30,57.32,21.9a67.88,67.88,0,0,1,69.49,9.26,74.14,74.14,0,0,1,27.12,46.13c.52,3,.85,5,1.07,6.31C155.6,86.78,155.33,87.29,155.33,86.38Z"></path>
            <path d="M155,83.6a20.66,20.66,0,0,1,.33,2.79C155.33,87.29,155.6,86.78,155,83.6Z"></path>
        </svg>
    </div>
    <div class="date">
        <?php print date('H:i'); ?>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>