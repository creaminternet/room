<?php

/************************************************
 The following 3 values an be found in the setting
 for the application you created on Google
 Developers console.	 	 Developers console.
 The Key file should be placed in a location
 that is not accessible from the web. outside of
 web root.

 In order to access your GA account you must
 Add the Email address as a user at the
 ACCOUNT Level in the GA admin.
 ************************************************/
$clientId = '';
$emailAddress = '';
$keyFileLocation = '';
